/*
 * Reference from Karen Code's Youtube Videos
 */
package memoryGame;

import java.awt.GridLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
/**
 * Class that represent the panel where the cards located
 */
public class GridPanel extends JPanel{
	
	static int numButtons;
	static String pics[] = {"/memoryGame/images/bear.png", "/memoryGame/images/boar.png",
							"/memoryGame/images/crocodile.png","/memoryGame/images/deer.png",
							"/memoryGame/images/dog.png","/memoryGame/images/duck.png",
							"/memoryGame/images/fox.png","/memoryGame/images/giraffe.png",
							"/memoryGame/images/hedgehog.png","/memoryGame/images/hippopotamus.png",
							"/memoryGame/images/kangaroo.png","/memoryGame/images/koala.png",
							"/memoryGame/images/lion.png","/memoryGame/images/owl.png",
							"/memoryGame/images/panda.png","/memoryGame/images/rhinoceros.png",
							"/memoryGame/images/wolf.png","/memoryGame/images/zebra.png"
		};
	static JButton[] buttons;
	ImageIcon cardBack = new ImageIcon(this.getClass().getResource("/memoryGame/images/zoo.png"));
	static ImageIcon[] icons;
	private ImageIcon temp;
	static int score = 0;
	
	Timer myTimer;
	Timer endTimer;
	static int openImages;
	static int attempts;
	public static int currentIndex;
	public static int oddClickIndex;
	public int numClicks;
	/**
	 * Constructor of the Grid Panel
	 */
	public GridPanel() {
		setBorder(new EmptyBorder(0,0,0,0));
		setLayout(new GridLayout(6,6,0,0));
		setBackground(Color.WHITE);
		setVisible(true);
		addButtons();
	}
	
	/**
	 * Method to add and randoming the button position
	 */
	private void addButtons() {
		numButtons = pics.length * 2;
		buttons = new JButton[numButtons];
		icons = new ImageIcon[numButtons];
		
		for(int i = 0, j = 0; i<pics.length; i ++) {
			icons[j] = new ImageIcon(this.getClass().getResource(pics[i]));
			j = makeButtons(j);
			
			icons[j] = icons[j - 1];
			j = makeButtons(j);
		}
		Random rnd = new Random ();
		for(int i =0; i<numButtons;i++) {
			int j = rnd.nextInt(numButtons);
			temp = icons[i];
			icons[i] = icons[j];
			icons[j] = temp;
		}
		myTimer = new Timer(500, new TimerListener());
		endTimer = new Timer(250, new AnotherTimer());
	}
	/**
	 * This ckass represent timer when different images are clicked
	 */
	private class TimerListener implements ActionListener {
		@Override
			public void actionPerformed(ActionEvent e) {
			buttons[currentIndex].setIcon(cardBack);
			buttons[oddClickIndex].setIcon(cardBack);
			myTimer.stop();
		}
	}
	/**
	 * This class represent timer that's called when the same images clicked
	 */
	private class AnotherTimer implements ActionListener {
		
		@Override
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {
			buttons[currentIndex].setVisible(false);
			buttons[oddClickIndex].setVisible(false);
			endTimer.stop();	
		}
	}
	/**
	 * Class that represent action when the buttons are pressed 
	 */
	private class ImageButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(myTimer.isRunning())
				return;
			openImages++;
			attempts = openImages/2;
			for(int i = 0; i< numButtons;i++) {
				if (e.getSource() == buttons[i]) {
					buttons[i].setIcon(icons[i]);
					currentIndex = i;
				}
			}
			
			if(openImages % 2 == 0) {// kalau sudah klik 2 kali
				//kalau klik yg sama
				if (currentIndex == oddClickIndex) {
					oddClickIndex = currentIndex;
					openImages--; // dianggap baru klik sekali
					return;
				}
				//kalo tidak match
				if(icons[currentIndex] != icons[oddClickIndex]) {
					//flip back
					myTimer.start();
				}
				else {
					score++;
					endTimer.start();
					ScorePanel.number.setText(Integer.toString(score));
				}
			}
			else {
				//kalau baru klik sekali
				oddClickIndex = currentIndex;
			}
			ScorePanel.numberAttempts.setText(Integer.toString(attempts)); // mengupdate total attempt ke label
		}
	}
	/**
	 * Method to make the cards
	 * @param j
	 * @return integer that represent the button ID
	 */
	private int makeButtons(int j) {
		buttons[j] = new JButton("");
		buttons[j].addActionListener(new GridPanel.ImageButtonListener());
		buttons[j].setIcon(cardBack);
		buttons[j].setBackground(Color.WHITE);
		add(buttons[j++]);
		return j;	
	}
	/**
	 * Method to reset the board
	 */
	public static void reset() {
		setAttempt(0);
		setScore(0);
		ScorePanel.numberAttempts.setText(Integer.toString(attempts));
		ScorePanel.number.setText(Integer.toString(score));		
		GameFrame game = new GameFrame();
		game.setVisible(true);
	}
	/**
	 * Method to set the score of current game
	 * @param score The value you want to set to score
	 */
	public static void setScore(int score) {
		GridPanel.score = score;
	}
	/**
	 * Method to get score of the game
	 * @return integer that represent the score of the game
	 */
	public static int getScore() {
		return score;
	}
	/**
	 * Method to get number of current attempt(s) in the game 
	 * @return integer that represent the current attempt(s) in the game
	 */
	public static int getAttempts() {
		return attempts;
	}
	/**
	 * Method to set the attempts of the game
	 * @param attempt The value you want to set to attempts
	 */
	public static void setAttempt(int attempt) {
		GridPanel.openImages = attempt;
		GridPanel.attempts = attempt;
	}
}
