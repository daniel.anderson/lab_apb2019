/*
 * Reference from Karen Code's Youtube Videos
 */
package memoryGame;

import java.awt.Color;
import java.awt.Toolkit;

import javax.swing.JFrame;
/**
 * A class that extends JFrame and represent the Game window
 */
public class GameFrame extends JFrame{
	/**
	 * 
	 */
	private static GamePanel gp;
	/**
	 * Game frame constructor				
	 */
	public MainMainFrame() {
		setTitle("Remember the Animals");
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(5,5,1200,1000);
		setIconImage(Toolkit.getDefaultToolkit()
		 		.getImage(GameFrame.class
		 		.getResource("/memoryGame/images/zooIcon.png")));
		gp = new GamePanel();
		setContentPane(gp);
	}
}
