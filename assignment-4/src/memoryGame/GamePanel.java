/*
 * Reference from Karen Code's Youtube Videos
 */
package memoryGame;

import java.awt.BorderLayout;

import javax.swing.JPanel;
/**
 * Class that become the place where the cards and the score panel located
 */
public class GamePanel extends JPanel{
	GridPanel gridPane;
	ScorePanel scorePane;
	
	/**
	 * Constructor of the Game Panel
	 */
	GamePanel(){
		setLayout(new BorderLayout(5,5));
		gridPane = new GridPanel();
		add(gridPane, BorderLayout.CENTER);
				
		scorePane = new ScorePanel();
		add(scorePane, BorderLayout.EAST);
		scorePane.setVisible(true);
		
	}
}
