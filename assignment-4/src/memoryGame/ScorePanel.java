/*
 * Reference from Karen Code's Youtube Videos
 */
package memoryGame;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
/**
 * Class that represent the panel with score, attempts, play again,and exit
 */
public class ScorePanel extends JPanel{
	JPanel labelScore;
	JPanel labelAttempt;
	JPanel buttonPanel;
	JLabel label;
	JLabel attemptsLabel;
	static JLabel number;
	static JLabel numberAttempts;
	JButton playAgain;
	JButton exitGame;
	
	/**
	 * Score Panel constructor
	 */
	public ScorePanel() {
		setBorder(new EmptyBorder(0,0,0,0));
		setLayout(new GridLayout(4,0,0,0));
		setBackground(Color.WHITE);
		setVisible(true);
		
		createPanel();
		add(labelScore);
		add(labelAttempt);
		add(playAgain);		
		add(exitGame);
		validate();
		repaint();
	}
	/**
	 * Method to create the score panel
	 */
	private void createPanel() {
		labelScore = new JPanel();
		labelScore.setLayout(new GridLayout(1,0,0,0));
		labelScore.setBackground(Color.WHITE);
		
		labelAttempt = new JPanel();
		labelAttempt.setLayout(new GridLayout(1,0,0,0));
		labelAttempt.setBackground(Color.WHITE);

		label = new JLabel("Number of Matches: ");
		number = new JLabel(" " + GridPanel.getScore());
		label.setHorizontalAlignment(JLabel.CENTER);
		number.setHorizontalAlignment(JLabel.CENTER);
		
		attemptsLabel = new JLabel("Number of Attempts: ");
		numberAttempts = new JLabel(" " +GridPanel.getAttempts());
		attemptsLabel.setHorizontalAlignment(JLabel.CENTER);
		numberAttempts.setHorizontalAlignment(JLabel.CENTER);
		
		playAgain = new JButton("Play Again?");
		playAgain.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GridPanel.reset();
			}
		});
		
		exitGame = new JButton ("Exit");
		exitGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);;	//exit the program
			}
		});
		
		labelScore.add(label);
		labelScore.add(number);
		
		labelAttempt.add(attemptsLabel);
		labelAttempt.add(numberAttempts);
	}
}
