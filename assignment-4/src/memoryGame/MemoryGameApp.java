/*
 * Reference from Karen Code's Youtube Videos
 */
package memoryGame;

import java.awt.EventQueue;

import javax.swing.JFrame;

/**
 * Class for Memory Game app
 */
public class MemoryGameApp extends JFrame {
/**
 * Main program for Memory Game
 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GameFrame game = new GameFrame();
					game.setVisible(true);
				}
				catch(Exception e) {
					e.printStackTrace();
					System.out.println("There's something wrong with the program");
				}
			}
		});

	}

}
